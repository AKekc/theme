
<?php

	/**
	 * Template Name: Home page
	 */

	get_header();


	// СТИЛИ ТАТУ
	?>
	<div id="content" class="site-content">
		
	<div class="row home-row ">
		<div class="container">
			<div class="row row-edge">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h1 style="text-align: center;">Стили тату</h1>
				</div>
			</div>
		</div>
	</div>
	<?php

	$posts = get_posts( array( 'category' => 18 ) );

	echo '<div class="container">
			<div class="row row-edge">';

	foreach ( $posts as $post ) {
		?>
		
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="cherry-banner template-banner_1 style_1 ">
							<div class="cherry-banner_wrap" style="background-color:#ffffff; color:#ffffff">
								<div class="banner_img">
									<?php printImage( get_the_post_thumbnail_url( $post->ID ), array( 'w' => 250, 'h' => 250 ) );?>
								</div>
								<a class="cherry-banner_link" href='<?php echo get_the_permalink( $post->ID ); ?>' >
									<div class="inner">
										<h2 class="cherry-banner_title" style="color:#ffffff;"><?php echo "$post->post_title";?></h2>
									</div>
									<div class="auxiliary">
									
								</div>
							</a>
						</div>
					</div>
				</div>
			
		
<?php 
	}

	echo '</div>
		</div>';

	?>
	<div class="col-md-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 col-xs-pull-0 col-sm-pull-0 col-md-pull-0 col-lg-pull-0 col-xs-push-0 col-sm-push-0 col-md-push-0 col-lg-push-0">
		<div class="row row-edge  ">
			<div class="col-md-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 col-xs-pull-0 col-sm-pull-0 col-md-pull-0 col-lg-pull-0 col-xs-push-0 col-sm-push-0 col-md-push-0 col-lg-push-0">
				<div class="aligncenter  -wrapper">
					<a class="cherry-btn cherry-btn-primary cherry-btn-medium cherry-btn-inline cherry-btn-fade   button-140" target="_self" href="http://vsetattoo.com.ua/tattoo-style/">Все стили тату
						<small class="cherry-btn-desc">перейти на страницу стилей</small>
					</a>
				</div>
			</div>
		</div>
		<div class="row row-edge  ">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="cherry-hr   hr-336"></div>
			</div>
		</div>
	</div>
<?php


	// СТИЛИ ТАТУ
	// 
	// 
	// ЗАПИСИ БЛОГА
		?>

		<?php

	$posts = get_posts( array( 'category' => 15 ) );

	echo '
		<div class="container">
		<div class="row  ">
			<div class="container">
				<div class="row row-edge">
					<div class="col-md-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 col-xs-pull-0 col-sm-pull-0 col-md-pull-0 col-lg-pull-0 col-xs-push-0 col-sm-push-0 col-md-push-0 col-lg-push-0">
						<div class="row row-edge  ">
							<div class="col-md-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 col-xs-pull-0 col-sm-pull-0 col-md-pull-0 col-lg-pull-0 col-xs-push-0 col-sm-push-0 col-md-push-0 col-lg-push-0">
								<div id="cherry-posts-list-1" class="cherry-posts-list template-default  ">
								';

	foreach ( $posts as $post ) {
		?>
		<div class="cherry-posts-item post-item item-0 odd col-sm-12" style="padding: 0">
										<div class="inner cherry-clearfix">
											<figure class="post-thumbnail">
												<a href="<?php echo get_the_permalink( $post->ID ); ?>">
													<?php printImage( get_the_post_thumbnail_url( $post->ID ), array( 'w' => 300, 'h' => 179 ) ); ?>
												</a>
											</figure>
										<h4 class="post-title">
											<a href="<?php echo get_the_permalink( $post->ID ) ?>"><?php echo $post->post_title;; ?></a>
										</h4>
										<div class="post-meta">
											Опубликовано <?php echo get_the_time('F j, Y', $post->ID );?> 
											<!--
											<span class="post-author vcard">
												<a href="http://vsetattoo.com.ua/author/madnessseogmail-com" rel="author">admin</a>
											</span>
											 <span class="post-tax post-tax-category">
												<a href="http://vsetattoo.com.ua/category/blog">Блог</a>
											</span>
											 -->
										</div>
										<div class="post-content part">
										<?php echo get_extended( $post->post_content )['main'];?>
										</div>
										<a href="<?php echo get_the_permalink( $post->ID ) ?>" class="btn btn-default">"Читать дальше"</a>
										<footer></footer>
										</div>
									</div><!--/.cherry-posts-item-->
		<?php
		
		// echo "<a style='display: inline-block;' href='" . get_the_permalink( $post->ID ) . "'>";

		// 	printImage( get_the_post_thumbnail_url( $post->ID ), array( 'w' => 300, 'h' => 179 ) );

		// 	echo $post->post_title;
		// 	echo "<br />";
		// 	echo get_extended( $post->post_content )['main'];
		// 	echo "<br />";
		// 	echo "<a href='" . get_permalink( $post->ID ) . "'>Читать дальше</a>";
		// 	echo get_the_time('F j, Y', $post->ID );

		// echo "</a>";

	}

	echo '
	</div><!--/.cherry-posts-list-->
							</div>
						</div>
						<div class="row row-edge  ">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="cherry-hr   hr-980"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<div class="cherry-hr   hr-336"></div>
			
		</div>
	</div>
	';

	// ЗАПИСИ БЛОГА
	// 
	// 
	// ЗНАЧЕНИЕ ТАТУ
	?>
	<div class="container">
	<div class="row row-edge">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<h2>Значения татуировок</h2>
</div>
</div>
</div>
	<?php
	
	$posts = get_posts( array( 'category' => 16 ) );

	echo '<div class="container">
			<div class="row row-edge">';

	foreach ( $posts as $post ) {
		?>
		
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="cherry-banner template-banner_1 style_1 ">
							<div class="cherry-banner_wrap" style="background-color:#ffffff; color:#ffffff">
								<div class="banner_img">
									<?php printImage( get_the_post_thumbnail_url( $post->ID ), array( 'w' => 250, 'h' => 250 ) );?>
								</div>
								<a class="cherry-banner_link" href='<?php echo get_the_permalink( $post->ID ); ?>' >
									<div class="inner">
										<h2 class="cherry-banner_title" style="color:#ffffff;"><?php echo "$post->post_title";?></h2>
									</div>
									<div class="auxiliary">
									
								</div>
							</a>
						</div>
					</div>
				</div>
			
		
<?php 
	}

	echo '</div>
			<div class="cherry-hr   hr-336"></div>
		</div>';

 ?>
<div class="col-md-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 col-xs-pull-0 col-sm-pull-0 col-md-pull-0 col-lg-pull-0 col-xs-push-0 col-sm-push-0 col-md-push-0 col-lg-push-0">
		<div class="row row-edge  ">
			<div class="col-md-12 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-0 col-xs-pull-0 col-sm-pull-0 col-md-pull-0 col-lg-pull-0 col-xs-push-0 col-sm-push-0 col-md-push-0 col-lg-push-0">
				<div class="aligncenter  -wrapper">
					<a class="cherry-btn cherry-btn-primary cherry-btn-medium cherry-btn-inline cherry-btn-fade   button-140" target="_self" 
					href='<?php echo get_category_link (3); ?>'>
					Посмотреть все значения
						<small class="cherry-btn-desc">Перейти на страницу с категориями значений</small>
					</a>
				</div>
			</div>
		</div>
	</div>



	</div>

<div class="container">
<div class="cherry-hr   hr-336"></div>
	<div class="row row-edge">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<h2>Последние публикации</h2>
</div>
</div>
</div>
	<?php
	
	$args = array(
			'numberposts' => 3,
			'category'         => 15,
			'post_status' => 'publish',
				); 
	echo '<div class="container">
			<div class="row row-edge">';
			$result = wp_get_recent_posts($args);

	foreach( $result as $p ){
		?>
		
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="cherry-banner template-banner_1 style_1 ">
							<div class="cherry-banner_wrap" style="background-color:#ffffff; color:#ffffff">
								<div class="banner_img">
									<?php printImage( get_the_post_thumbnail_url( $p['ID']), array( 'w' => 254, 'h' => 152 ) ); ?>
								</div>
								 
								<a class="cherry-banner_link" href="<?php echo get_permalink($p['ID']) ?>">
									<div class="inner">
										<h2 class="cherry-banner_title" style="color:#ffffff;"></h2>
									</div>
									<div class="auxiliary">
									
								</div>
							</a>
						</div>
					</div>
					<a href="<?php echo get_permalink($p['ID']) ?>">
					<div class="inner">
						<h2 class="cherry-banner_title"><?php echo $p['post_title'];?></h2>
					</div>
					</a>
				</div>
			
		
<?php 
	}

	echo '</div>
			<div class="cherry-hr   hr-336"></div>
		</div>';

 ?>
 

 <?php


	// ЗНАЧЕНИЕ ТАТУ

	get_footer();

?>

