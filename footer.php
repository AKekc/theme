	<footer>

	 	<span id="footer" class="site-footer wide">
	<?php
				if ( function_exists('dynamic_sidebar') ){
					?>

		<div id="static-area-footer-top" class="footer-top static-area">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 static-footer-sidebars">
						<div class="row">
							<div class="col-xs-12 col-sm-3">
								<div class="sidebar-footer-1 widget-area" role="complementary">
									<aside id="text-5" class="widget widget_text">
									 
										<div class="textwidget">
											<p><?php dynamic_sidebar('sidebar-1'); ?> </p>
										</div>
									</aside>
								</div>
							</div>
							<div class="col-xs-12 col-sm-3">
								<div class="sidebar-footer-2 widget-area" role="complementary">
									<aside id="nav_menu-7" class="widget widget_nav_menu">
									 	<?php dynamic_sidebar('sidebar-2'); ?>
									</aside>
								</div>
							</div>
							<div class="col-xs-12 col-sm-3">
								<div class="sidebar-footer-3 widget-area" role="complementary">
									<aside id="nav_menu-9" class="widget widget_nav_menu">
									<?php dynamic_sidebar('sidebar-3'); ?>
									</aside>
								</div>
							</div>
						<div class="col-xs-12 col-sm-3">
							<div class="sidebar-footer-4 widget-area" role="complementary">
								<aside id="cherry-instagram-3" class="widget cherry-instagram_widget">
 									 	<?php dynamic_sidebar('sidebar-4'); ?>
									</aside>
								</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="static-area-footer-bottom" class="footer-bottom static-area">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 static-footer-info">
						<div class="site-info">© 2017. </div>
					</div>
				</div>
			</div>
		</div>

	<?php
	 }
	 ?>
	

	</span>

	</footer>
	<?php wp_footer(); ?>
	</body>
</html>