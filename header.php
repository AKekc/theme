<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php echo get_bloginfo( 'name' ); ?></title>
		<?php wp_head(); ?>
	</head>
	<body>

	<header id="header" class="site-header wide" role="banner">

		<div id="static-area-header-top" class="header-top static-area">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 static-header-logo">
						<div class="site-branding">
							<h1 class="site-title image-logo">
								<?php the_custom_logo(); ?>

								<!-- <a href="http://vsetattoo.com.ua/" rel="home">
									<img src="http://vsetattoo.com.ua/wp-content/uploads/2016/10/logo-vsetatto.png" alt="Vse Tattoo">
								</a> -->
							</h1> 
							<div class="site-description">
								<!-- Олезные материалы для настоящих ценителей тату. -->
								<?php echo get_bloginfo( 'description' ); ?>

							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 static-search-form">
							<?php get_search_form(); ?>

						<!-- <form role="search" method="get" class="search-form" action="http://vsetattoo.com.ua/">
								<label>
									<span class="screen-reader-text">Найти:</span>
									<input type="search" class="search-field" placeholder="Поиск…" value="" name="s">
								</label>
								<input type="submit" class="search-submit" value="Поиск">
							</form> -->
						</div>
						<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 static-header-sidebar"></div>
					</div>
				</div>
			</div>





		<div id="static-area-header-bottom" class="header-bottom static-area">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 static-header-menu">

						<nav role="navigation" class="">
						 <div class="navbar-header">
						    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
						      <span class="xs-only my_toggle">Menu</span>
						    </button>
						    
						    
						  </div>

						  <div id="navbarCollapse" class="collapse navbar-collapse">
						  	<?php
						
							wp_nav_menu( array(
								'theme_location'  => 'Primary Menu',
								'menu'            => 'HeaderGeneralMenu', 
								'container'       => 'div', 
								'container_class' => '', 
								'container_id'    => '',
								'menu_class'      => 'nav navbar-nav', 
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 0,
								'walker'         => new Bootstrap_Walker_Nav_Menu(),
							) ); 



						?>
						<button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle" aria-expanded="true">
						      <span class="xs-only my_toggle_close">Close</span>
						    </button>
						   
						  </div>
						</nav>


				 
					</div>
				</div>
			</div>
		</div>



		<div id="static-area-showcase-area" class="showcase-area static-area">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 static-moto-slider moto-slider">
						<?php echo custom_breadcrumbs(); ?>
						
					</div>
				</div>
			</div>
		</div>
		</header>


<!-- 
		<nav id="access" role="navigation">
			<?php wp_nav_menu( 'HeaderGeneralMenu' ); ?>
		</nav> -->

<style>
	.moto-slider {
    position: relative;
    padding-top: 20px;
    padding-bottom: 20px;
}

.moto-slider:before {
    background: #282e34;
}

#breadcrumbs{
	margin: 0;
}
.bread-home:before {
    display: inline-block;
    vertical-align: top;
    line-height: 22px;
    font-family: FontAwesome;
    font-size: 20px;
    content: '\f015';
    margin-right: 20px;
    }

</style>