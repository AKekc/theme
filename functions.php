<?php 

    /**
	 * Enqueue scripts
	 */
	function karmaTattoo() {
		wp_enqueue_style( 
			'bootstrap',
			'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', 
			array(), 
			'3.3.7'
		);
		wp_enqueue_style( 
			'style',
			get_stylesheet_directory_uri() . "/assets/style.css", 
			array(), 
			'2.28'
		);
		wp_enqueue_style( 
			'main',
			get_stylesheet_directory_uri() . "/assets/main.css", 
			array(), 
			'2.28'
		);
		wp_enqueue_script( 'jquery' );

		wp_enqueue_script( 
			'bootstrap',
			'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', 
			array(), 
			'2.28'
		);
		wp_enqueue_script( 
			'fontawesome',
			'https://use.fontawesome.com/13cebad2b0.js', 
			array(), 
			'2.28'
		);

	}

	add_action( 'wp_enqueue_scripts', 'karmaTattoo' );
	/**
	 * КОНЕЦ
	 */

	/**
	 * Моддержка меню в теме
	 */
	// add_theme_support( 'menus' );
	function theme_register_nav_menu() {

		register_nav_menu( 'primary', 'Primary Menu' );

	}

	add_action( 'after_setup_theme', 'theme_register_nav_menu' );

 
	class Bootstrap_Walker_Nav_Menu extends Walker_Nav_Menu {
	 
	  /**
	   * Display Element
	   */
	  function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
	    $id_field = $this->db_fields['id'];
	 
	    if ( isset( $args[0] ) && is_object( $args[0] ) )
	    {
	      $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
	 
	    }
	 
	    return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	  }
	 
	  /**
	   * Start Element
	   */
	  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
	    if ( is_object($args) && !empty($args->has_children) )
	    {
	      $link_after = $args->link_after;
	      $args->link_after = ' <b class="fa fa-angle-down mega-menu-arrow top-level-arrow"></b>';
	    }
	 
	    parent::start_el($output, $item, $depth, $args, $id);
	 
	    if ( is_object($args) && !empty($args->has_children) )
	      $args->link_after = $link_after;
	  }
	 
	  /**
	   * Start Level
	   */
	  function start_lvl( &$output, $depth = 0, $args = array() ) {
	    $indent = str_repeat("t", $depth);
	    $output .= "\n$indent<ul class=\"dropdown-menu list-unstyled\">\n";
	  }
	}
	
	add_filter('nav_menu_link_attributes', function($atts, $item, $args) {
	  if ( $args->has_children )
	  {
	    $atts['data-toggle'] = 'dropdown';
	    $atts['class'] = 'dropdown-toggle';
	  }
	 
	  return $atts;
	}, 10, 3);

	/**
	 * КОНЕЦ
	 */

	/**
	 * Рубрики для страниц
	 */
	add_theme_support( 'post-thumbnails' );
	/**
	 * КОНЕЦ
	 */
	
	/**
	 * Поддержка логотипа
	 */
	add_theme_support( 'custom-logo' );
	/**
	 * КОНЕЦ
	 */
	
	/**
	 * Поддержка виджетов
	 */
	if ( function_exists( 'register_sidebar' ) ) {

		add_action( 'widgets_init', 'register_my_widgets' );

	}

	function register_my_widgets(){

		// Кексик, это стили что бы распознать виджет.
		$be    = '<div class="karma-widget karma-widget-';
		$fore  = '">';
		$after = '</div>';

		register_sidebars( 
			4,
			array(
				'name'          => 'Footer widget #%d',
				'before_widget' => $be.$fore,
				'after_widget'  => $after,
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>'
			)
		);

	}
if ( function_exists( 'register_sidebar' ) ) {

    add_action( 'widgets_init', 'register_right_widgets' );

}

function register_right_widgets(){

    // Кексик, это стили что бы распознать виджет.
    $be    = '<div class="karma-widget karma-widget-';
    $fore  = '">';
    $after = '</div>';

    register_sidebars(
        2,
        array(
            'name'          => 'Right widget #%d',
            'before_widget' => $be.$fore,
            'after_widget'  => $after,
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>'
        )
    );

}
	

	/**
	 * КОНЕЦ
	 */
	
	/**
	 * Убираем префикс category из адресной строки
	 */
	add_filter('user_trailingslashit', 'remcat_function');

	function remcat_function( $link ) {
	    return str_replace( "/category/", "/", $link );
	}
	  
	add_action( 'init', 'remcat_flush_rules' );

	function remcat_flush_rules() {

	    global $wp_rewrite;
	    $wp_rewrite->flush_rules();

	}
	  
	add_filter('generate_rewrite_rules', 'remcat_rewrite');

	function remcat_rewrite( $wp_rewrite ) {

	    $new_rules = array( '(.+)/page/(.+)/?' => 'index.php?category_name=' .
	    $wp_rewrite->preg_index(1) . '&paged=' . $wp_rewrite->preg_index(2) );
	    $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;

	}
	/**
	 * КОНЕЦ
	 */

	/**
	 * Выводим картинку если она есть
	 * $image => string
	 * $size => array. In px.
	 */
	function printImage( $image, $size ) {

		if( ! empty( $image ) )
			echo "<img style='width: " . $size['w'] . "px; height: " . 
				$size['h'] . "px; ' src='" . $image . "' />";
		else 
			echo "<img style='width: " . $size['w'] . "px; height: " . 
				$size['h'] . "px; display: block; margin-right: 100px;' src='"
				. get_stylesheet_directory_uri() . 
				"/assets/imagenotfound.svg" . "' />";

	}
	/**
	 * КОНЕЦ
	 */
	
	// Breadcrumbs
	function custom_breadcrumbs() {
	       
	    // Settings
	    $separator          = '|';
	    $breadcrums_id      = 'breadcrumbs';
	    $breadcrums_class   = 'breadcrumbs';
	    $home_title         = '';
	      
	    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
	    $custom_taxonomy    = '';
	       
	    // Get the query & post information
	    global $post,$wp_query;
	       
	    // Do not display on the homepage
	    if ( !is_front_page() ) {
	       
	        // Build the breadcrums
	        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
	           
	        // Home page
	        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
	        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
	           
	        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
	              
	            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
	              
	        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
	              
	            // If post is a custom post type
	            $post_type = get_post_type();
	              
	            // If it is a custom post type display name and link
	            if($post_type != 'post') {
	                  
	                $post_type_object = get_post_type_object($post_type);
	                $post_type_archive = get_post_type_archive_link($post_type);
	              
	                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
	                echo '<li class="separator"> ' . $separator . ' </li>';
	              
	            }
	              
	            $custom_tax_name = get_queried_object()->name;
	            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
	              
	        } else if ( is_single() ) {
	              
	            // If post is a custom post type
	            $post_type = get_post_type();
	              
	            // If it is a custom post type display name and link
	            if($post_type != 'post') {
	                  
	                $post_type_object = get_post_type_object($post_type);
	                $post_type_archive = get_post_type_archive_link($post_type);
	              
	                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
	                echo '<li class="separator"> ' . $separator . ' </li>';
	              
	            }
	              
	            // Get post category info
	            $category = get_the_category();
	             
	            if(!empty($category)) {
	              
	                // Get last category post is in
	                $last_category = end(array_values($category));
	                  
	                // Get parent any categories and create array
	                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
	                $cat_parents = explode(',',$get_cat_parents);
	                  
	                // Loop through parent categories and store in variable $cat_display
	                $cat_display = '';
	                foreach($cat_parents as $parents) {
	                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
	                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
	                }
	             
	            }
	              
	            // If it's a custom post type within a custom taxonomy
	            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
	            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
	                   
	                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
	                $cat_id         = $taxonomy_terms[0]->term_id;
	                $cat_nicename   = $taxonomy_terms[0]->slug;
	                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
	                $cat_name       = $taxonomy_terms[0]->name;
	               
	            }
	              
	            // Check if the post is in a category
	            if(!empty($last_category)) {
	                echo $cat_display;
	                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
	                  
	            // Else if post is in a custom taxonomy
	            } else if(!empty($cat_id)) {
	                  
	                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
	                echo '<li class="separator"> ' . $separator . ' </li>';
	                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
	              
	            } else {
	                  
	                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
	                  
	            }
	              
	        } else if ( is_category() ) {
	               
	            // Category page
	            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
	               
	        } else if ( is_page() ) {
	               
	            // Standard page
	            if( $post->post_parent ){
	                   
	                // If child page, get parents 
	                $anc = get_post_ancestors( $post->ID );
	                   
	                // Get parents in the right order
	                $anc = array_reverse($anc);
	                   
	                // Parent page loop
	                if ( !isset( $parents ) ) $parents = null;
	                foreach ( $anc as $ancestor ) {
	                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
	                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
	                }
	                   
	                // Display parent pages
	                echo $parents;
	                   
	                // Current page
	                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
	                   
	            } else {
	                   
	                // Just display current page if not parents
	                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
	                   
	            }
	               
	        } else if ( is_tag() ) {
	               
	            // Tag page
	               
	            // Get tag information
	            $term_id        = get_query_var('tag_id');
	            $taxonomy       = 'post_tag';
	            $args           = 'include=' . $term_id;
	            $terms          = get_terms( $taxonomy, $args );
	            $get_term_id    = $terms[0]->term_id;
	            $get_term_slug  = $terms[0]->slug;
	            $get_term_name  = $terms[0]->name;
	               
	            // Display the tag name
	            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
	           
	        } elseif ( is_day() ) {
	               
	            // Day archive
	               
	            // Year link
	            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
	            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
	               
	            // Month link
	            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
	            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
	               
	            // Day display
	            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
	               
	        } else if ( is_month() ) {
	               
	            // Month Archive
	               
	            // Year link
	            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
	            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
	               
	            // Month display
	            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
	               
	        } else if ( is_year() ) {
	               
	            // Display year archive
	            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
	               
	        } else if ( is_author() ) {
	               
	            // Auhor archive
	               
	            // Get the author information
	            global $author;
	            $userdata = get_userdata( $author );
	               
	            // Display author name
	            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
	           
	        } else if ( get_query_var('paged') ) {
	               
	            // Paginated archives
	            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
	               
	        } else if ( is_search() ) {
	           
	            // Search results page
	            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
	           
	        } elseif ( is_404() ) {
	               
	            // 404 page
	            echo '<li>' . 'Error 404' . '</li>';
	        }
	       
	        echo '</ul>';
	           
	    }
	       
	}


	function my_search_form( $form ) {

	$form = '


<form role="search" method="get" class="search-form" action="' . home_url( '/' ) . '">
				<label>
					<span class="screen-reader-text">Найти:</span>
					<input type="search" class="search-field" placeholder="Поиск…" value="' . get_search_query() . '" name="s">
				</label>
				<input type="submit" class="search-submit" value="Поиск">
			</form>
'; 



	return $form;
}

add_filter( 'get_search_form', 'my_search_form' );

add_action('category_template', 'delfi_load_cat_parent_template');
function delfi_load_cat_parent_template($template) {
    $cat_ID = absint( get_query_var('cat') );
    $category = get_category( $cat_ID );
    if($category->category_parent > 0) {
        $templates = array();
            
        if(!is_wp_error($category)) {
            $templates[] = "category-{$category->slug}.php";
        }
        $templates[] = "category-$cat_ID.php";
        
        $parentCategory = get_category($category->category_parent);
        if(!is_wp_error($parentCategory)) {
            $templates[] = "subcategory-{$parentCategory->slug}.php";
            $templates[] = "subcategory-{$parentCategory->term_id}.php";
        }
        
        $templates[] = "category.php";
        $template = locate_template($templates);
    }
    return $template;
}

// single для категорий по ID

add_action('customize_register','my_customize_register');
function my_customize_register( $wp_customize ) {
    // Section
    $wp_customize->add_section('tattoo_color_section', array(
        'title' => __('Цвет фона', STEPBYSTEP_THEME_TEXTDOMAIN),
        'priority' => 30,
        'description' => __('Здесь вы можете изменить цвет фона сайта', STEPBYSTEP_THEME_TEXTDOMAIN),
    ));
    // Setting
    $wp_customize->add_setting("tattoo_my_color_settings", array(
        "default" => "",
        "transport" => "postMessage",
    ));
    // Control
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'tattoo_my_color_settings',
            array(
                'label'      => __( 'Color', STEPBYSTEP_THEME_TEXTDOMAIN),
                'section'    => 'tattoo_color_section',
                'settings'   => 'tattoo_my_color_settings'
            )
        )
    );
}

add_action( 'wp_head', 'my_custom_css_output');
function my_custom_css_output() {
    ?>

    <style type="text/css" id="custom-theme-css">
        body { background: <?php echo get_theme_mod( 'tattoo_my_color_settings' ); ?>; }

    </style>
    <?php


}
add_action( 'customize_preview_init', 'my_customizer_script' );
function my_customizer_script() {
    wp_enqueue_script(
        'my-customizer-script',
        get_template_directory_uri().'/assets/js/my-customizer-script.js', //$src,
        array( 'jquery', 'customize-preview' )
    );
}


?>