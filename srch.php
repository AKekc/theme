<?php get_header(); ?>

<div id="content" class="site-content boxed extra-boxed">
	<div class="container">

		<div id="primary" class="content-area"><main id="main" class="site-main" role="main"><article class="cherry-has-entry-date cherry-has-entry-author cherry-has-entry-comments cherry-has-entry-cats cherry-has-entry-tags clearfix post-2079 page type-page status-publish has-post-thumbnail hentry" id="post-2079"><figure class="entry-thumbnail cherry-thumb-l alignnone large"><a href="http://vsetattoo.com.ua/znachenie-tattoo/magic/krest-i-drakon" title="Крест с драконом"><img width="300" height="300" src="http://vsetattoo.com.ua/wp-content/uploads/2017/02/tattoo-cross-and-dragon-preview.jpg" class="attachment-cherry-thumb-l size-cherry-thumb-l wp-post-image" alt="фото тату крест с драконом" srcset="http://vsetattoo.com.ua/wp-content/uploads/2017/02/tattoo-cross-and-dragon-preview.jpg 300w, http://vsetattoo.com.ua/wp-content/uploads/2017/02/tattoo-cross-and-dragon-preview-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px"></a></figure>
<header class="entry-header"><h2 class="entry-title"><a href="http://vsetattoo.com.ua/znachenie-tattoo/magic/krest-i-drakon" rel="bookmark">Крест с драконом</a></h2></header>

<div class="entry-meta entry-meta-top">
	<span class="posted-on">Опубликованный  <time class="entry-date published" datetime="2017-02-17T17:30:41+00:00">Февраль 17, 2017</time></span>
	<span class="author vcard"> <a href="http://vsetattoo.com.ua/author/madnessseogmail-com" title="Записи admin" rel="author">admin</a></span>
	
</div>

<div class="entry-content"></div>
<div class="entry-permalink"><a href="http://vsetattoo.com.ua/znachenie-tattoo/magic/krest-i-drakon" class="btn btn-default">Читать дальше</a></div></article>
	</main></div>
				
	</div>
</div>
 




	<div id="page">

		<div class="column span-9 first" id="maincontent">

			<div class="content">

	<?php if (have_posts()) : ?>

		<h2 class="pagetitle">Search Results for "<?php echo $s ?>"</h2>

		<div class="clear"></div>

		<?php while (have_posts()) : the_post(); ?>

			<div class="post" id="post-<?php the_ID(); ?>">

				<?php
					if( ! empty( get_the_post_thumbnail_url() ) )
						echo "<img style='width: 200px; height: 200px; display: block; margin-right: 100px;' src='" . 
							get_the_post_thumbnail_url() . "' /></br>";
					else 
						echo "<img style='width: 200px; height: 200px; display: block; margin-right: 100px;' src='" . 
							get_stylesheet_directory_uri() . "/assets/imagenotfound.svg" . "' /></br>";
				?>

				<p class="large nomargin">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></p>
				<?php
				// Support for "Search Excerpt" plugin
				// http://fucoder.com/code/search-excerpt/
				if ( function_exists('the_excerpt') && is_search() ) {
					the_excerpt();
				} ?>
				<p class="small">
					<?php the_time('F jS, Y') ?> &nbsp;|&nbsp; 
					<!-- by <?php the_author() ?> -->
					Published in
					<?php the_category(', ');
						if($post->comment_count > 0) { 
								echo ' &nbsp;|&nbsp; ';
								comments_popup_link('', '1 Comment', '% Comments'); 
						}
					?>
				</p>
				
			</div>
			
			<hr>
		<?php endwhile; ?>

	<?php else : ?>

		<h2 class="center">No posts found. Try a different search?</h2>

	<?php endif; ?>

		</div> <!-- /content -->
	</div> <!-- /maincontent-->

	<?php // get_sidebar(); ?>

	</div> <!-- /page -->

<?php get_footer(); ?>