<?php

	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$posts = get_posts( array( 'category' => the_category_ID( false ), 'posts_per_page' => 1, 'paged' => $paged ) );


	//$posts = get_posts( array( 'category' => 6 ) );

?>

<div id="content" class="site-content boxed extra-boxed">
	<div class="container">

		<div class="sidebar-content-wrapper">



		<div id="primary" class="content-area col-sm-9">
					<?php
						foreach ( $posts as $post ) {
					?>
				<main id="main" class="site-main" role="main">
					<article class="cherry-has-entry-date cherry-has-entry-author cherry-has-entry-comments cherry-has-entry-cats cherry-has-entry-tags clearfix post-1597 post type-post status-publish format-standard has-post-thumbnail hentry category-blog" id="post-1597"><div style="width: 100%">
						<div class="k_thumb" style="width: 29%;display: inline-block;margin-right: 3%;vertical-align: top;">
							<figure class="entry-thumbnail cherry-thumb-l alignnone large">
								<a href='<?php echo get_the_permalink( $post->ID ); ?>' title="Шрамирование — что такое и в чем особенность">
									<?php printImage( get_the_post_thumbnail_url( $post->ID ), array( 'w' => 254, 'h' => 152 ) ); ?>
								</a>
							</figure>
						</div>
						<div class="k_cont" style="width: 65%; display: inline-block; vertical-align: top;">
							<header class="entry-header">
								<h2 class="entry-title">
									<a href='<?php echo get_the_permalink( $post->ID ); ?>' rel="bookmark"><?php echo $post->post_title;; ?></a>
								</h2>
							</header>
							<div class="entry-content"><?php echo get_extended( $post->post_content )['main'];?></div>
							<div style="width: 100%;">
								<div style="text-align: left; width: 49%; display: inline-block;">
									<div class="entry-permalink">
										<a href='<?php echo get_the_permalink( $post->ID ) ?>' class="btn btn-default">Читать дальше</a>
									</div>
								</div>
								<div style="text-align: right; width: 49%; display: inline-block;">
									<span class="posted-on">
										<time class="entry-date published" ><?php echo get_the_time('F j, Y', $post->ID );?></time>
									</span>
								</div>
							</div>
						</div>
					</div>
					<style>
					@media screen and (max-width: 1024px)  {
					  .k_thumb, .k_cont{
					width: 95%!important;
						}
					}

					</style>
					</article>

				</main>
				<?php } ?>
				<?php the_posts_pagination( array(
    'mid_size' => 1,
    'prev_text' => __( 'Back', 'textdomain' ),
    'next_text' => __( 'Onward', 'textdomain' ),
) ); ?>

<?php

 ?>
			</div>

			<div class="cherry-sidebar-main sidebar-main widget-area col-sm-4" role="complementary" style="/* text-align: center; */">
				<aside id="categories-3" class="widget widget_categories">

					<div class="karma-widget karma-widget-">

					<?php
					if ( function_exists('dynamic_sidebar') ){
						dynamic_sidebar('Right widget #1');
						}
						 ?>
						 <?php
							$args = array(
								'numberposts' => 3,

								'post_status' => 'publish',
							);

							$result = wp_get_recent_posts($args);

							foreach( $result as $p ){


							}
						?>

					</div>
				</aside>
			</div>





		</div>
	</div>

</div>