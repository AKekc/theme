<?php get_header(); ?>

	<div  id="content" class="site-content boxed extra-boxed">

		<div class="container">

			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

	<?php if (have_posts()) : ?>

		<h2 class="pagetitle">Результаты поиска по запросу "<?php echo $s ?>"</h2>

		<div class="clear"></div>

		<?php while (have_posts()) : the_post(); ?>
				<article class="cherry-has-entry-date cherry-has-entry-author cherry-has-entry-comments cherry-has-entry-cats cherry-has-entry-tags clearfix post-2079 page type-page status-publish has-post-thumbnail hentry" id="post-2079">
					<figure class="entry-thumbnail cherry-thumb-l alignnone large">
						<a href="http://vsetattoo.com.ua/znachenie-tattoo/magic/krest-i-drakon" title="Крест с драконом">
						<?php
					if( ! empty( get_the_post_thumbnail_url() ) )
						echo "<img class='attachment-cherry-thumb-l size-cherry-thumb-l wp-post-image' src='" . 
							get_the_post_thumbnail_url() . "' /></br>";
					else 
						echo "<img class='attachment-cherry-thumb-l size-cherry-thumb-l wp-post-image'  src='" . 
							get_stylesheet_directory_uri() . "/assets/imagenotfound.svg" . "' /></br>";
				?>
							
						</a>
					</figure>
					<header class="entry-header">
						<h2 class="entry-title">
							<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
					</header>

					<div class="entry-meta entry-meta-top">
						 
						<?php
				// Support for "Search Excerpt" plugin
				// http://fucoder.com/code/search-excerpt/
				if ( function_exists('the_excerpt') && is_search() ) {
					the_excerpt();
				} ?>
				<time >
					<?php the_time('F jS, Y') ?> &nbsp;|&nbsp; 
					<!-- by <?php the_author() ?> -->
						<span class="author vcard"><?php the_author() ?> </span>

					Опубликовано в 
					<?php the_category(', ');
						if($post->comment_count > 0) { 
								echo ' &nbsp;|&nbsp; ';
								comments_popup_link('', '1 Comment', '% Comments'); 
						}
					?>
				</time>
							<!-- <time class="entry-date published" datetime="2017-02-17T17:30:41+00:00">Февраль 17, 2017</time> -->
					 
						
					</div>

					<div class="entry-content"></div>
					<div class="entry-permalink">
						<a href="<?php the_permalink() ?>" class="btn btn-default">Читать дальше</a>
					</div>
				</article>

 
			
			<hr>
		<?php endwhile; ?>
<?php the_posts_pagination( array(
    'mid_size' => 1,
    'prev_text' => __( 'Назад', 'textdomain' ),
    'next_text' => __( 'Вперед', 'textdomain' ),
) ); ?>
	<?php else : ?>

		<h2 class="center">Ничего не найдено,попробуйте другой поисковый запрос</h2>

	<?php endif; ?>
				</main>
			</div> <!-- /content -->
		</div> <!-- /maincontent-->
	</div> <!-- /page -->

<?php get_footer(); ?>