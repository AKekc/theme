<?php
	
	get_header();

	if( the_category_ID( false ) && the_category_ID( false ) > 0 ) {

		if( the_category_ID( false ) == 6 ) {

			include_once( 'blog_style.php' );

		}
		elseif (the_category_ID( false ) == 5) {
			include_once( 'news_style.php' );
		 	} else {
 


			$posts = get_posts( array( 'category' => the_category_ID( false ) ) );


	echo '<div class="container">
			<div class="row row-edge">';

	foreach ( $posts as $post ) {
		?>

		
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="cherry-banner template-banner_1 style_1 ">
							<div class="cherry-banner_wrap" style="background-color:#ffffff; color:#ffffff">
								<div class="banner_img banner_img_category">
									<?php printImage( get_the_post_thumbnail_url( $post->ID ), array( 'w' => 250, 'h' => 250 ) );?>
								</div>
								<a class="cherry-banner_link" href='<?php echo get_the_permalink( $post->ID ); ?>' >
									<div class="inner">
										<h2 class="cherry-banner_title" style="color:#ffffff;"><?php echo "$post->post_title";?></h2>
									</div>
									<div class="auxiliary">
									
								</div>
							</a>
						</div>
					</div>
				</div>
			
		
<?php 
	}

	echo '</div>
		</div>';


		}

	} else {
		_e( 'Простите, в этой категории пока нет постов!' );
	}

	get_footer();

?>