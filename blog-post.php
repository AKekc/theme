<?php

	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$posts = get_posts( array( 'category' => the_category_ID( false ), 'posts_per_page' => 1, 'paged' => $paged ) );

	
	//$posts = get_posts( array( 'category' => 6 ) );
 
?>

<div id="content" class="site-content boxed extra-boxed">
	<div class="container">

		<div class="sidebar-content-wrapper">
		
		

		<div id="primary" class="content-area col-sm-9">
					<?php
						foreach ( $posts as $post ) {
					?>
				<main id="main" class="site-main" role="main">
					<article class="cherry-has-entry-date cherry-has-entry-author cherry-has-entry-comments cherry-has-entry-cats cherry-has-entry-tags clearfix post-1597 post type-post status-publish format-standard has-post-thumbnail hentry category-blog" id="post-1597">

					<div class="container-blog_post">
						<?php 
						if ( have_posts() ) :  
								 		while ( have_posts() ) : the_post(); 
											echo '<div class="container-blog_post">';
											the_content();
											echo '</div>';
									 	endwhile; 
									 endif; 
						?>
						</div>
					<style>
					@media screen and (max-width: 1024px)  {
					  .k_thumb, .k_cont{
					width: 95%!important;
						}
					}
						
					</style>
					</article>
					
				</main>
				<?php } ?>
				<?php the_posts_pagination( array(
    'mid_size' => 1,
    'prev_text' => __( 'Back', 'textdomain' ),
    'next_text' => __( 'Onward', 'textdomain' ),
) ); ?>

<?php 

 ?>
			</div>

			<div class="cherry-sidebar-main sidebar-main widget-area col-sm-4 " role="complementary" style="/* text-align: center; */">
				<aside id="categories-3" class="widget widget_categories">
					 	
					<div class="karma-widget karma-widget-">
						
					<?php 
					if ( function_exists('dynamic_sidebar') ){
						dynamic_sidebar('sidebar-5');
						}
						 ?>
						 <div class="blog_post">
						 <?php 
							$args = array(
								'numberposts' => 3,
								'category'         => 6,
								'post_status' => 'publish',
							); 

							$result = wp_get_recent_posts($args);

							foreach( $result as $p ){ 
								 
							?>
							<div class="col-sm-12">
								<div class="blog_post-img">
								<?php printImage( get_the_post_thumbnail_url( $p['ID']), array( 'w' => 254, 'h' => 152 ) ); ?>
								</div>

								<a href="<?php echo get_permalink($p['ID']) ?>"><?php echo $p['post_title'] ?></a><br />
								  
								</div>   
							<?php 
							} 
						?>
						</div>

					</div>
				</aside>
			</div>

			<div class="col-sm-12">
			<div class="container">
	<div class="row row-edge">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<div class="cherry-hr   hr-336"></div>
<h2>Вам будет интересно</h2>
</div>
</div>
</div>
				<?php
	
	$posts = get_posts( array( 'category' => 3 ) );

	echo '<div class="container">
			<div class="row row-edge">';

	foreach ( $posts as $post ) {
		?>
		
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<div class="cherry-banner template-banner_1 style_1 ">
							<div class="cherry-banner_wrap" style="background-color:#ffffff; color:#ffffff">
								<div class="banner_img">
									<?php printImage( get_the_post_thumbnail_url( $post->ID ), array( 'w' => 250, 'h' => 250 ) );?>
								</div>
								<a class="cherry-banner_link" href='<?php echo get_the_permalink( $post->ID ); ?>' >
									<div class="inner">
										<h2 class="cherry-banner_title" style="color:#ffffff;"><?php echo "$post->post_title";?></h2>
									</div>
									<div class="auxiliary">
									
								</div>
							</a>
						</div>
					</div>
				</div>
			
		
<?php 
	}

	echo '</div>
			<div class="cherry-hr   hr-336"></div>
		</div>';

 ?>
 

	</div>



			</div>

			  
				
			 
			
		</div>		
	</div>
	
</div>